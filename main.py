from tkinter import *
import math

# ---------------------------- CONSTANTS ------------------------------- #
PINK = "#e2979c"
RED = "#e7305b"
GREEN = "#9bdeac"
YELLOW = "#f7f5dd"
FONT_NAME = "Courier"
WORK_MIN = 25
SHORT_BREAK_MIN = 5  # after each work for 3
LONG_BREAK_MIN = 20  # after 4th WORK
repetitions = 0
timer = None


# ---------------------------- TIMER RESET ------------------------------- #
def reset_timer():
    global timer
    global repetitions
    repetitions = 0
    window.after_cancel(timer)
    canvas.itemconfig(timer_text, text="00:00")
    title_label.config(text="Timer")
    check_label.config(text="")


# ---------------------------- TIMER MECHANISM ------------------------------- #
def on_start_button_clicked():
    print("start btn clicked")
    global repetitions
    repetitions += 1

    work_sec = WORK_MIN * 60
    short_break_sec = SHORT_BREAK_MIN * 60
    long_break_sec = LONG_BREAK_MIN * 60

    # 8                 =>  LONG BREAK
    if repetitions % 8 == 0:
        title_label.config(text="Break", fg=RED)
        count_down(long_break_sec)
    # 2 - 4 - 6         =>  SHORT BREAK
    elif repetitions % 2 == 0:
        title_label.config(text="Break", fg=PINK)
        count_down(short_break_sec)
    # 1 - 3 - 5 - 7     =>  WORK
    else:
        title_label.config(text="Work", fg=GREEN)
        count_down(work_sec)


# ---------------------------- COUNTDOWN MECHANISM ------------------------------- #
def count_down(count):
    global repetitions
    count_minutes = math.floor(count / 60)
    count_seconds = count % 60
    if count_seconds < 10:
        count_seconds = f"0{count_seconds}"
    display = f"{count_minutes}:{count_seconds}"
    canvas.itemconfig(timer_text, text=display)
    if count > 0:
        global timer
        timer = window.after(1000, count_down, count - 1)
    else:
        on_start_button_clicked()
        check_mark_txt = ""
        for i in range(0, math.floor(repetitions / 2)):
            check_mark_txt += "✔"

        check_label.config(text=check_mark_txt)


# ---------------------------- UI SETUP ------------------------------- #
window = Tk()
window.title("Pomodoro App")
window.config(padx=100, pady=50, bg=YELLOW)

canvas = Canvas(width=202, height=226, bg=YELLOW, highlightthickness=0)
tomato_img = PhotoImage(file='tomato.png')
canvas.create_image(102, 112, image=tomato_img)
timer_text = canvas.create_text(102, 130, text="00:00", fill="white", font=(FONT_NAME, 30, "bold"))
canvas.grid(column=1, row=1)

title_label = Label(text="Timer", bg=YELLOW, fg=GREEN, font=(FONT_NAME, 40, "bold"), highlightthickness=0)
title_label.grid(column=1, row=0)

start_button = Button(text="Start", command=on_start_button_clicked)
start_button.grid(column=0, row=2)

reset_button = Button(text="Reset", command=reset_timer)
reset_button.grid(column=2, row=2)

check_label = Label(fg=GREEN, bg=YELLOW, font=(FONT_NAME, 15, "bold"))
check_label.grid(column=1, row=3)

window.mainloop()
